const moment = require('moment')

// Services
const redis = require('../services/redis')
const elasticsearch = require('../services/elasticsearch')

const index = async function(req, res) {
	try {
		let { device_data } = req

		if(device_data) {
			device_data = JSON.parse(device_data)
			const cities = device_data.user_cities

			const size = parseInt(req.query.limit) || 20
			const page = parseInt(req.query.page) || 1
			const from = (page - 1) * size

			const elastic_search_query = {
				index: 'memories',
				body:{
					query:{
						bool:{
							must: [ 
								{
									match: {
										status: 'Published'
									}
								},
								{
									range:{
										publish_datetime:{
											lte: moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')
										}
									}
								},
								{
									range:{
										end_datetime:{
											gte: moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')
										}
									}
								}
							]
						}
					},
					sort:{
						publish_datetime:{
							order: 'desc'
						}
					}
				},
				from,
				size
			}

			const response = await elasticsearch.search(elastic_search_query)

			const total = response['hits']['total']['value']
			const last_page = Math.round(total / size)

			const data = {
				server_time: +new Date(),
				status: true,
				data: [],
				elastic_search: true,
				node: true,
				pagination: {
					show_pagination: true,
					total,
					per_page: size,
					current_page: page,
					last_page,
					from: from + 1,
					to: from + size
				}
			}

			for (const raw_memory of response.hits.hits) {
				const memory = raw_memory._source
				if(memory.reactions !== undefined) {
					memory.reacted = (memory.reactions.indexOf(device_data.device.id) !== -1)? 1: 0
				} else {
					memory.reactions_count = 0
					memory.reacted = 0
				}
				if(memory.shares !== undefined) {
					memory.shared = (memory.shares.indexOf(device_data.device.id) !== -1)? 1: 0
				} else {
					memory.shares_count = 0
					memory.shared = 0
				}
				delete memory.cities
				delete memory.reactions
				delete memory.shares

				memory.death_date = moment(memory.death_date, 'YYYY-MM-DD').format('DD-MM-YYYY')

				if(memory.birth_date !== null) {
					memory.birth_date = moment(memory.birth_date, 'YYYY-MM-DD').format('DD-MM-YYYY')
				}

				data.data.push(memory)
			}

			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

module.exports = {
	index
}