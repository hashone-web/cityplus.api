const moment = require('moment')

// Services
const redis = require('../services/redis')
const elasticsearch = require('../services/elasticsearch')

// Helpers
const ads = require('../helpers/ads')
const poll = require('../helpers/poll')
const config = require('../helpers/config')

const index = async function(req, res) {
	try {
		let { device_data } = req

		if(device_data) {
			device_data = JSON.parse(device_data)
			const cities = device_data.user_cities

			const now_date_only = moment().utcOffset(330).format('YYYY-MM-DD')
			const now = moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')

			const size = parseInt(req.query.limit) || 20
			const page = parseInt(req.query.page) || 1
			const from = (page - 1) * size

			const { category } = req.query

			const elastic_search_query = {
				index: 'news',
				body: {
					query: {
						bool:{
							must: [
								{
									match: {
										status: 'Published'
									}
								},
								{
									range: {
										datetime: {
											lte: now
										}
									}
								}
							]
						}
					},
					sort: {
						datetime: {
							order: 'desc'
						}
					}
				},
				from,
				size
			}

			if(req.query.last_request_ts !== undefined) {
				elastic_search_query['body']['query']['bool']['must'].push({
					range: {
						datetime: {
							gte: moment.unix(req.query.last_request_ts / 1000).utcOffset(330).format('YYYY-MM-DD HH:mm:ss')
						}
					}
				})
			}

			const category_id = category !== undefined? parseInt(category): 0;

			const dummy_categories_ids = Object.values(config.categories[process.env.APP_ENV]);

			if(category_id && dummy_categories_ids.indexOf(category_id) == -1) {
				elastic_search_query['body']['query']['bool']['filter'] = [
					{
						terms: {
							categories_ids: [ category_id ]
						}
					}
				]
			}

			if(category_id == config.categories[process.env.APP_ENV]['trending']) {
				delete elastic_search_query['body']['query']['bool']['filter']
				
				elastic_search_query['body']['query']['bool']['must'].push({
					match: {
						is_featured: 1	
					}
				})
			}

			const response = await elasticsearch.search(elastic_search_query)

			const total = response['hits']['total']['value']
			const last_page = Math.ceil(total / size)

			const data = {
				server_time: +new Date(),
				status: true,
				data: [],
				poll: null,
				elastic_search: true,
				node: true,
				pagination: {
					show_pagination: true,
					total,
					per_page: size,
					current_page: page,
					last_page,
					from: from + 1,
					to: from + size
				}
			}

			data.poll = await poll.get(device_data.device, now)

			let ads_track = 0
			for (const raw_news of response.hits.hits) {
				const news = raw_news._source
				let reaction = null
				if(news.likes !== undefined && news.likes.indexOf(device_data.device.id) !== -1) {
					reaction = 'Claps'
				}
				if(news.dislikes !== undefined && news.dislikes.indexOf(device_data.device.id) !== -1) {
					reaction = 'Sad'
				}
				news.reaction = reaction

				let is_sensitive_viewed = req.query.platform !== undefined && req.query.platform == 'iOS' ? 0: null
				if(news.sensitivities !== undefined && news.sensitivities.indexOf(device_data.device.id) !== -1) {
					is_sensitive_viewed = 1
				}
				news.is_sensitive_viewed = is_sensitive_viewed

				delete news.categories_ids
				delete news.cities
				delete news.v2_cities
				delete news.likes
				delete news.dislikes
				delete news.sensitivities
				delete news.created_by_user
				delete news.updated_by_user
				delete news.notifications
				delete news.cities_with_details
				delete news.categories_with_details
				delete news.media_views
				delete news.media_downloads

				ads_track++

				if(ads_track >= 6) {
					if(Math.round(Math.random())) {
						news.online_banner = 1
						news.online_ads = 1
						news.online_ads_type = Math.round(Math.random()) === 0? 'native': 'rectangle'
					} else {
						const banner = await ads.get('feed', device_data.user_cities, device_data.device, now_date_only)

						if(banner) {
							news.banner = banner
						} else {
							news.online_banner = 1
							news.online_ads = 1
							news.online_ads_type = Math.round(Math.random()) === 0? 'native': 'rectangle'
						}
					}
					
					ads_track = 0
				}

				data.data.push(news)
			}

			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

const useful = async function(req, res) {
	try {
		let { device_data } = req

		if(device_data) {
			const now = moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')

			device_data = JSON.parse(device_data)
			const cities = device_data.user_cities

			const size = parseInt(req.query.limit) || 20
			const page = parseInt(req.query.page) || 1
			const from = (page - 1) * size

			const elastic_search_query = {
				index: 'news',
				body: {
					query: {
						bool:{
							must: [
								{
									match: {
										status: 'Published'
									}
								},
								{
									match: {
										is_useful:1
									}
								},
								{
									range: {
										datetime: {
											lte: now
										}
									}
								}
							],
							should: [
								{
									range: {
										is_useful_expiry_datetime: {
											gte: now
										}
									}
								},
								{
									bool: {
										must_not: {
											exists: {
												field: "is_useful_expiry_datetime"
											}
										}
									}
								}
							]
						}
					},
					sort: {
						datetime: {
							order: 'desc'
						}
					}
				},
				from,
				size
			}

			const response = await elasticsearch.search(elastic_search_query)

			const total = response['hits']['total']['value']
			const last_page = Math.round(total / size)

			const data = {
				server_time: +new Date(),
				status: true,
				data: [],
				poll: null,
				elastic_search: true,
				node: true,
				pagination: {
					show_pagination: true,
					total,
					per_page: size,
					current_page: page,
					last_page,
					from: from + 1,
					to: from + size
				}
			}

			for (const raw_news of response.hits.hits) {
				const news = raw_news._source
				let reaction = null
				if(news.likes !== undefined && news.likes.indexOf(device_data.device.id) !== -1) {
					reaction = 'Claps'
				}
				if(news.dislikes !== undefined && news.dislikes.indexOf(device_data.device.id) !== -1) {
					reaction = 'Sad'
				}
				news.reaction = reaction

				let is_sensitive_viewed = req.query.platform !== undefined && req.query.platform == 'iOS' ? 0: null
				if(news.sensitivities !== undefined && news.sensitivities.indexOf(device_data.device.id) !== -1) {
					is_sensitive_viewed = 1
				}
				news.is_sensitive_viewed = is_sensitive_viewed

				if(req.platform == 'iOS') {
					news.article = news.article_text !== null? news.article_text: news.article
				}

				delete news.article_text
				delete news.cities
				delete news.v2_cities
				delete news.likes
				delete news.dislikes
				delete news.sensitivities
				data.data.push(news)
			}

			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

const single = async function(req, res) {
	try {
		let device_data = await redis.get('cityplus_database_device_data:' + req.query.device_id)

		if(device_data) {
			device_data = JSON.parse(device_data)

			const now_date_only = moment().utcOffset(330).format('YYYY-MM-DD')
			const now = moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')

			const elastic_search_query = {
				index: 'news',
				id: req.params.id
			}

			const response = await elasticsearch.get(elastic_search_query)

			const data = {
				server_time: +new Date(),
				status: true,
				data: null,
				elastic_search: true,
				node: true,
			}

			const news = response._source
			let reaction = null
			if(news.likes !== undefined && news.likes.indexOf(device_data.device.id) !== -1) {
				reaction = 'Claps'
			}
			if(news.dislikes !== undefined && news.dislikes.indexOf(device_data.device.id) !== -1) {
				reaction = 'Sad'
			}
			news.reaction = reaction

			let is_sensitive_viewed = req.query.platform !== undefined && req.query.platform == 'iOS' ? 0: null
			if(news.sensitivities !== undefined && news.sensitivities.indexOf(device_data.device.id) !== -1) {
				is_sensitive_viewed = 1
			}
			news.is_sensitive_viewed = is_sensitive_viewed

			if(req.platform == 'iOS') {
				news.article = news.article_text !== null? news.article_text: news.article
			}

			delete news.article_text
			delete news.cities
			delete news.v2_cities
			delete news.likes
			delete news.dislikes
			delete news.sensitivities

			news.poll = null
			news.banner = await ads.get('detail', device_data.user_cities, device_data.device, now_date_only)

			data.data = news
			
			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

module.exports = {
	index,
	useful,
	single
}