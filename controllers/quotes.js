const moment = require('moment')

// Services
const redis = require('../services/redis')
const elasticsearch = require('../services/elasticsearch')

const today = async function(req, res) {
	try {
		let { device_data } = req

		if(device_data) {
			device_data = JSON.parse(device_data)

			const elastic_search_query = {
				index: 'quotes',
				body:{
					query:{
						bool:{
							must: [
								{
									range:{
										date:{
											lte: moment().utcOffset(330).format('YYYY-MM-DD')
										}
									}
								}
							]
						}
					},
					sort:{
						date:{
							order: 'desc'
						}
					}
				},
				from: 0,
				size: 1
			}

			const response = await elasticsearch.search(elastic_search_query)

			const data = {
				server_time: +new Date(),
				status: false,
				elastic_search: true,
				node: true
			}

			for (const raw_quote of response.hits.hits) {
				const quote = raw_quote._source
				data.status = true

				if(quote.reactions !== undefined) {
					quote.reacted = (quote.reactions.indexOf(device_data.device.id) !== -1)? 1: 0
				} else {
					quote.reactions_count = 0
					quote.reacted = 0
				}
				if(quote.shares !== undefined) {
					quote.shared = (quote.shares.indexOf(device_data.device.id) !== -1)? 1: 0
				} else {
					quote.shares_count = 0
					quote.shared = 0
				}
				delete quote.reactions
				delete quote.shares

				data.data = quote
			}

			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

module.exports = {
	today
}