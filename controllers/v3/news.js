const moment = require('moment')

// Services
const redis = require('../../services/redis')
const elasticsearch = require('../../services/elasticsearch')

// Helpers
const ads = require('../../helpers/ads')
const poll = require('../../helpers/poll')
const config = require('../../helpers/config')

const index = async function(req, res) {
	try {
		let { device_data } = req

		if(device_data) {
			device_data = JSON.parse(device_data)
			const cities = device_data.user_cities

			const now_date_only = moment().utcOffset(330).format('YYYY-MM-DD')
			const now = moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')

			const size = parseInt(req.query.limit) || 20
			const page = parseInt(req.query.page) || 1
			const from = (page - 1) * size

			const { category } = req.query

			const elastic_search_query = {
				index: 'news',
				body: {
					query: {
						bool:{
							must: [
								{
									match: {
										status: 'Published'
									}
								},
								{
									range: {
										datetime: {
											lte: now
										}
									}
								}
							]
						}
					},
					sort: {
						datetime: {
							order: 'desc'
						}
					}
				},
				from,
				size
			}

			if(req.query.last_request_ts !== undefined) {
				elastic_search_query['body']['query']['bool']['must'].push({
					range: {
						datetime: {
							gte: moment.unix(req.query.last_request_ts / 1000).utcOffset(330).format('YYYY-MM-DD HH:mm:ss')
						}
					}
				})
			}

			if(req.query.news_date) {
				const start_date = moment(req.query.news_date, 'YYYY-MM-DD').utcOffset(330).format('YYYY-MM-DD HH:mm:ss')
				const end_date = moment(req.query.news_date, 'YYYY-MM-DD').add('+1', 'days').utcOffset(330).format('YYYY-MM-DD HH:mm:ss')
				elastic_search_query['body']['query']['bool']['must'] = [
					{
						match: {
							status: 'Published'
						}
					},
					{
						range: {
							datetime: {
								gte: start_date,
								lt: end_date
							}
						}
					}
				]
			}

			const category_id = category !== undefined? parseInt(category): 0;

			const dummy_categories_ids = Object.values(config.categories_v3[process.env.APP_ENV]);

			if(category_id && dummy_categories_ids.indexOf(category_id) == -1) {
				elastic_search_query['body']['query']['bool']['filter'] = [
					{
						terms: {
							categories_ids: [ category_id ]
						}
					}
				]
			}

			if(category_id == config.categories[process.env.APP_ENV]['trending']) {
				delete elastic_search_query['body']['query']['bool']['filter']
				
				elastic_search_query['body']['query']['bool']['must'].push({
					match: {
						is_featured: 1	
					}
				})
			}

			const response = await elasticsearch.search(elastic_search_query)

			const total = response['hits']['total']['value']
			const last_page = Math.ceil(total / size)

			const data = {
				server_time: +new Date(),
				server_date: now_date_only,
				status: true,
				data: [],
				poll: null,
				elastic_search: true,
				node: true,
				pagination: {
					show_pagination: true,
					total,
					per_page: size,
					current_page: page,
					last_page,
					from: from + 1,
					to: from + size
				}
			}

			data.poll = await poll.get(device_data.device, now)

			let ads_track = 0
			for (const raw_news of response.hits.hits) {
				const news = raw_news._source
				let reaction = null
				if(news.likes !== undefined && news.likes.indexOf(device_data.device.id) !== -1) {
					reaction = 'Claps'
				}
				if(news.dislikes !== undefined && news.dislikes.indexOf(device_data.device.id) !== -1) {
					reaction = 'Sad'
				}
				news.reaction = reaction

				let is_sensitive_viewed = req.query.platform !== undefined && req.query.platform == 'iOS' ? 0: null
				if(news.sensitivities !== undefined && news.sensitivities.indexOf(device_data.device.id) !== -1) {
					is_sensitive_viewed = 1
				}
				news.is_sensitive_viewed = is_sensitive_viewed

				delete news.categories_ids
				delete news.cities
				delete news.v2_cities
				delete news.likes
				delete news.dislikes
				delete news.sensitivities
				delete news.created_by_user
				delete news.updated_by_user
				delete news.notifications
				delete news.cities_with_details
				delete news.categories_with_details
				delete news.media_views
				delete news.media_downloads

				ads_track++

				if(ads_track >= 6) {
					if(Math.round(Math.random())) {
						news.online_banner = 1
						news.online_ads = 1
						news.online_ads_type = Math.round(Math.random()) === 0? 'native': 'rectangle'
					} else {
						const banner = await ads.get('feed', device_data.user_cities, device_data.device, now_date_only)

						if(banner) {
							news.banner = banner
						} else {
							news.online_banner = 1
							news.online_ads = 1
							news.online_ads_type = Math.round(Math.random()) === 0? 'native': 'rectangle'
						}
					}
					
					ads_track = 0
				}

				data.data.push(news)
			}

			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

module.exports = {
	index
}