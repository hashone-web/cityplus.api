const moment = require('moment')

// Services
const pool = require('../services/mysql')

const index = async function(req, res) {
	try {
		let { device_data } = req

		if(device_data) {
			device_data = JSON.parse(device_data)

			const size = parseInt(req.query.limit) || 20
			const page = parseInt(req.query.page) || 1
			const from = (page - 1) * size

			const [categories] = await pool.query('select * from categories where status=1 and deleted_at is null order by sort asc limit ' + size + ' offset ' + from)
			const [categories_total] = await pool.query('select count(id) as total from categories where status=1 and deleted_at is null')

			const total = categories_total[0].total
			const last_page = Math.ceil(total / size)

			for (const category of categories) {
				category.image_full_path = process.env.APP_URL + '/storage/categories/'+ category.id +'/image/256px/' + category.image
			}

			const data = {
				server_time: +new Date(),
				status: true,
				data: categories,
				elastic_search: false,
				node: true,
				pagination: {
					show_pagination: true,
					total,
					per_page: size,
					current_page: page,
					last_page,
					from: from + 1,
					to: from + size
				}
			}

			res.json(data)
		} else {
			throw 'device not found'
		}
	} catch(error) {
		res.json({
			status: false,
			error
		})
	}
}

module.exports = {
	index
}