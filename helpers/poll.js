const moment = require('moment')

// Services
const pool = require('../services/mysql')

async function get(device, datetime) {
	const [polls] = await pool.query('select * from polls where type="Standalone" and status="Active" and start_datetime < "' + datetime + '" and (end_datetime > "' + datetime + '" or (show_result != "Never" and result_end_datetime > "' + datetime + '")) and polls.deleted_at is null limit 1')

	if(polls.length) {
		const poll = polls[0]
		poll.poll_id = poll.id;
		
		const [poll_result_device_views] = await pool.query('select count(id) as is_result_viewed FROM poll_result_device_views where poll_id='+ poll.id +' and device_id=' + device.id)
		poll.is_result_viewed = poll_result_device_views[0].is_result_viewed;

		poll.votable = poll.end_datetime > datetime? 1:0;
		poll.time_label = getPollTimeLabel(poll.end_datetime);

		const [poll_options] = await pool.query('select * from poll_options where poll_options.poll_id in ('+ poll.id +') and poll_options.deleted_at is null')
		const poll_option_ids = poll_options.map(poll_option => poll_option.id).join(',')

		const [device_poll_options] = await pool.query('select * from device_poll_options where poll_option_id in ('+ poll_option_ids +') and device_id =' + device.id)
		const device_poll_option_ids = device_poll_options.map(device_poll_option => device_poll_option.poll_option_id)

		if(poll.start_datetime < datetime && (poll.end_datetime > datetime || (poll.result_end_datetime !== null && poll.result_end_datetime > datetime))) {
			poll_options.forEach(poll_option => {
				poll_option.is_selected = 0
				if(device_poll_option_ids.length && device_poll_option_ids.indexOf(poll_option.id) !== -1) {
					poll_option.is_selected = 1
				}
			})

			poll.options = poll_options

			poll.is_voted = 0
			if(device_poll_options.length) {
				poll.is_voted = 1
			}

			return poll
		}
	}

	return null
}

function getPollTimeLabel(end_datetime) {
	const start_time = moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss');
	const end_time = end_datetime;
	
	const start_time_obj = moment().utcOffset(330);
	const end_time_obj = moment(end_time, 'YYYY-MM-DD HH:mm:ss');
	
	const minutes = (moment(end_time, 'YYYY-MM-DD HH:mm:ss').valueOf() - start_time_obj) / (60 * 1000);

	const diff_time = moment.duration(end_time_obj.diff(start_time_obj));

	let time_label = 'Poll ended';
	if(minutes) {
		let z = 'left to vote';

		if(minutes >= 1440) {
			let d = diff_time.days();
			let h = diff_time.hours();
			let di =  d > 1? 'days': 'day';
			
			time_label = `${d} ${di}`;
			
			if(h) {
				let hi =  h > 1? 'hours': 'hour';
				time_label = time_label + ` ${h} ${hi}`;
			} else {
				let i = diff_time.minutes();
				if(i) {
					let yi =  i > 1? 'minutes': 'minute';
					time_label = time_label + ` ${i} ${yi}`;
				}
			}

			time_label = time_label + ` ${z}`;
		} else if(minutes < 1440 && minutes >= 60) {
			let h = diff_time.hours();
			let i = diff_time.minutes();
			let hi =  h > 1? 'hours': 'hour';
			
			time_label = `${h} ${hi}`;
			
			if(i) {
				let yi =  i > 1? 'minutes': 'minute';
				time_label = time_label + ` ${i} ${yi}`;
			}
			
			time_label = time_label + ` ${z}`;
		} else if(minutes < 60 && minutes > 1) {
			let i = diff_time.minutes();
			let yi =  i > 1? 'minutes': 'minute';
			time_label = `${i} ${yi} ${z}`;
		} else {
			time_label = 'ending soon';
		}
	}
	
	return time_label;
}

module.exports = {
	get
}