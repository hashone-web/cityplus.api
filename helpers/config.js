module.exports = {
	'categories': {
        'production': {
            'feed': 10,
            'trending': 11
        },
        'test': {
            'feed': 3,
            'trending': 9
        },
        'local': {
            'feed': 18,
            'trending': 19
        }
    },
    'categories_v3': {
        'production': {
            'home': 1
        },
        'test': {
            'home': 1
        },
        'local': {
            'home': 1
        }
    },
}