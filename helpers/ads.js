const moment = require('moment')

// Services
const pool = require('../services/mysql')
const elasticsearch = require('../services/elasticsearch')

async function get(type, cities, device, datetime) {
	const elastic_search_query = {
		index: 'ads',
		body: {
			query: {
				bool: {
					must: [ 
						{
							match: {
								campaign_status: 'Active'
							}
						},
						{
							match: {
								type: type
							}
						},
						{
							match: {
								campaign_deleted_at: 0
							}
						},
						{
							range: {
								campaign_start_date: {
									lte: datetime
								}
							}
						},
						{
							range: {
								campaign_end_date: {
									gte: datetime
								}
							}
						}
					]
				}
			},
			sort : {
				_script : { 
					script : 'Math.random()',
					type : 'number',
					order : 'asc'
				}
			}
		},
		size: 1
	}

	const response = await elasticsearch.search(elastic_search_query)

	let banner = null

    if(response['hits']['total']['value']) {
        banner = response['hits']['hits'][0]['_source']
        
        if(banner['reactions'] == undefined) {
            banner['reactions'] = []
            banner['reactions_count'] = 0
            banner['reacted'] = 0
        } else {
            banner['reacted'] = banner['reactions'].indexOf(device.id) !== -1? 1: 0
        }
        
        if(banner['shares'] == undefined) {
            banner['shares'] = []
            banner['shares_count'] = 0
            banner['shared'] = 0
        } else {
            banner['shared'] = banner['shares'].indexOf(device.id) !== -1? 1: 0
        }

        const now = moment().utcOffset(330).format('YYYY-MM-DD HH:mm:ss')

        const [rows] = await pool.query('select * from campaign_device_impressions where device_id='+ device.id +' and campaign_id='+ banner['campaign_id'] +' and campaign_banner_id='+ banner['id'] +' and impression_date="'+ datetime +'" limit 1');
		if(rows.length === 0) {
			await pool.query('insert into campaign_device_impressions (device_id, campaign_id, campaign_banner_id, impressions, impression_date, updated_at, created_at) values ('+ device.id +', '+ banner['campaign_id'] +', '+ banner['id'] +', 1, "'+ datetime +'", "'+ now +'", "'+ now +'")')
		} else {
			await pool.query('update campaign_device_impressions set impressions = impressions + 1, campaign_device_impressions.updated_at="'+ now +'" where id = '+ rows[0].id)
		}
    }

    return banner
}

module.exports = {
	get
}