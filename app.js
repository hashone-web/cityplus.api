require('dotenv').config()

const express = require('express')
const app = express()
const port = process.env.PORT || 3002

const router = express.Router()
const router_v3 = express.Router()


// Middlewares
const auth = require('./middlewares/auth')
const device_data = require('./middlewares/device_data')


// Routes imports
const categories = require('./routes/categories')
const memories = require('./routes/memories')
const quotes = require('./routes/quotes')
const news = require('./routes/news')
const news_v3 = require('./routes/v3/news')

// Routes setup
// V2 APIS
router.use('/categories', auth, device_data, categories)
router.use('/memories', auth, device_data, memories)
router.use('/quotes', auth, device_data, quotes)
router.use('/news', auth, device_data, news)
app.use('/api/v2', router)

// V3 APIS
router_v3.use('/news', auth, device_data, news_v3)
app.use('/api/v3', router_v3)

// Index Page
app.get('/', (req, res) => {
	res.send('Welcome to CityPlus')
})

app.use((req, res, next) => {
	res.status(404)

	res.send({
		'status': false,
		'error': 'Not found'
	})
})

app.listen(port, () => {
	console.log(`Cityplus API server listening on port ${port}`)
})