const elasticsearch = require('elasticsearch')

class ElasticSearch {
	constructor() {
		this.client = new elasticsearch.Client({
			host: 'localhost:9200',
			log: 'error',
			apiVersion: '7.x', // use the same version of your Elasticsearch instance
		})
	}

	search(query) {
		return this.client.search(query)
	}

	get(query) {
		return this.client.get(query)
	}
}

module.exports = new ElasticSearch