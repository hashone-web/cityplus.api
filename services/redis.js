const redis = require('redis') 
const { promisify } = require('util')

class Redis {
	constructor() {
		this.client = redis.createClient()
		this.redisGetAsync = promisify(this.client.get).bind(this.client)
		this.redisSetAsync = promisify(this.client.set).bind(this.client)
	}

	get(key) {
		return this.redisGetAsync(key)
	}

	set(key, value) {
		return this.redisSetAsync(key, value)
	}
}

module.exports = new Redis

