// Services
const pool = require('../services/mysql')
const redis = require('../services/redis')

const device_data = async function (req, res, next) {
	req.platform = req.query.platform? req.query.platform: 'Android'

	req.device_data = await redis.get('cityplus_database_device_data:' + req.query.device_id);

	if(req.device_data === null) {
		try {
			const [rows] = await pool.query('SELECT devices.id, devices.unique_id, GROUP_CONCAT(device_cities.city_id) AS cities from devices LEFT JOIN device_cities ON devices.id = device_cities.device_id where unique_id="'+ req.query.device_id +'" GROUP BY devices.id');

			if(rows.length === 0) {
				throw 'device not found'
			} else {
				const device = rows[0]
				
	            const device_data = {
	                'device': { 
	                    'id': device.id,
	                    'unique_id': device.unique_id,
	                },
	                'user_cities': device.cities !== null? device.cities.split(',').map(city_id => parseInt(city_id)): []
	            }

	            req.device_data = JSON.stringify(device_data)

				await redis.set('cityplus_database_device_data:' + device.unique_id, req.device_data);
			
				next()
			}
		} catch(error) {
			res.json({
				status: false,
				error
			})
		}
	} else {
		next()
	}
}

module.exports = device_data