const auth = function (req, res, next) {
	if(req.query.device_id == undefined) {
		res.send({
			'error': {
				'device_id': [
					'The device id field is required.'
				]
			}
		})
	}
	if(req.query.token == undefined) {
		res.send({
			'error': {
				'token': [
					'The token field is required.'
				]
			}
		})
	}

	let token = req.query.token
	token = new Buffer.from(token, 'base64');
    token = token.toString('ascii');
    token = token.substr(5);
    token = token.substr(0, token.length - 6);
    const token_first_five = token.substr(0, 5);
    const token_after_nine = token.substr(9);
    var final_token = token_first_five + token_after_nine;
    final_token = new Buffer.from(final_token, 'base64');
    decoded_token = final_token.toString('ascii');
	if(
		req.query.device_id &&
		(
			decoded_token == 'com.hashone.applicationtemplate' ||
			decoded_token == 'com.cityplus' ||
			decoded_token == 'com.cityplus.notification.test' ||
			decoded_token == 'com.cityplus.local.news.info' ||
			decoded_token == 'com.cityplus.local.news.info.test' ||
			decoded_token == 'com.cityplus.local.news.info.test.one' ||
			decoded_token == 'com.cityplus.local.news.info.gopal'
		)
	) {
		next()
	} else {
		res.send({
			'status': false,
			'message': 'authentication failed'
		})
	}
}

module.exports = auth