const express = require('express')
const router = express.Router()
const memories = require('../controllers/memories')

router.get('/', memories.index)

module.exports = router