const express = require('express')
const router = express.Router()
const news = require('../../controllers/v3/news')

router.get('/', news.index)

module.exports = router