const express = require('express')
const router = express.Router()
const news = require('../controllers/news')

router.get('/', news.index)
router.get('/useful', news.useful)
router.get('/:id', news.single)

module.exports = router